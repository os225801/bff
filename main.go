package main

import (
	"bff/config"
	"bff/internal/authenticator"
	"bff/internal/enum"
	"bff/internal/logger"
	"bff/internal/middleware"
	"bff/internal/route/authen"
	"bff/internal/route/service"
	"bff/internal/utils"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"io"
)

func init() {
	// Setup Gin
	gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = io.Discard
	// Load configs
	config.LoadAllConfig(enum.EnvironmentFile)
}

func main() {
	// Get app config and Register a new router
	appCfg := config.AppCfg()
	router := gin.Default()

	// Config CORS
	corsConfig := cors.Config{
		AllowOrigins:     []string{appCfg.CorsOriginProd, appCfg.CorsOriginDev, appCfg.CorsOriginModerator},
		AllowMethods:     enum.CorsAllowMethods,
		AllowHeaders:     enum.CorsAllowHeaders,
		AllowCredentials: true,
		MaxAge:           enum.CorsMaxAge,
	}
	router.Use(cors.New(corsConfig))

	// Create logger
	l := logger.Get()

	// Setup OIDC
	auth, err := authenticator.New()
	if err != nil {
		utils.StartUpError(l, enum.SetupOidcFailed)
	}

	// Register request logger
	router.Use(middleware.RequestLogger())

	// Load all routes
	LoadRoutes(auth, router, appCfg)

	// Run service
	serverAddr := fmt.Sprintf("%s:%d", appCfg.Host, appCfg.Port)
	l.Info("Server is listening on " + serverAddr)
	if err := router.Run(serverAddr); err != nil {
		utils.StartUpError(l, enum.ApplicationStartFailed)
	}
}

func LoadRoutes(auth *authenticator.Authenticator, router *gin.Engine, appCfg *config.App) {
	// Authenticate Routes
	authen.Authenticate(auth, router, appCfg)

	// Services Routes
	service.PostRoutes(router, appCfg)
	service.ArtworkRoutes(router, appCfg)
	service.CommentRoutes(router, appCfg)
	service.LikeRoutes(router, appCfg)
	service.TagRoutes(router, appCfg)
	service.UserRoutes(router, appCfg)
	service.WalletRoutes(router, appCfg)
	service.OrderRoutes(router, appCfg)
	service.TransactionRoutes(router, appCfg)
	service.FollowRoutes(router, appCfg)
	service.CollectionRoutes(router, appCfg)
}
