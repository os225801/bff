package cache

import (
	"bff/internal/enum"
	"bff/internal/logger"
	"bff/internal/utils"
	"github.com/maypok86/otter"
)

var Store otter.CacheWithVariableTTL[string, string]

func init() {
	var err error
	Store, err = otter.MustBuilder[string, string](10_000).
		CollectStats().
		Cost(func(key string, value string) uint32 {
			return 1
		}).
		WithVariableTTL().
		Build()
	if err != nil {
		l := logger.Get()
		utils.StartUpError(l, enum.CacheStoreStartFailed)
	}
}
