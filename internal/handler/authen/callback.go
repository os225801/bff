package authen

import (
	"bff/config"
	"bff/internal/authenticator"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/model/service"
	"bff/internal/utils"
	"crypto/rand"
	"encoding/base32"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

var moderatorIds = []string{
	"google-oauth2|103166434261305612272",
	"google-oauth2|113925467274043092374",
	"google-oauth2|105253065271146105844",
	"google-oauth2|106419772215855868049",
	"google-oauth2|110060233010385481654",
	"google-oauth2|106341284430726518572"}

func CallBackHandler(auth *authenticator.Authenticator, appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {

		token, err := auth.Exchange(context.Request.Context(), context.Query(enum.OidcExchangeCode))
		if err != nil {
			utils.LogRequestError(context, enum.ExchangeOidcTokenFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}
		idToken, err := auth.VerifyIdToken(context.Request.Context(), token)
		if err != nil {
			utils.LogRequestError(context, enum.VerifyIdTokenFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}

		var profile map[string]interface{}
		if err := idToken.Claims(&profile); err != nil {
			utils.LogRequestError(context, enum.GetUserProfileFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}

		userId := profile[enum.OidcUserId].(string)
		if userId == "" {
			utils.LogRequestError(context, enum.GetUserIdFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}

		getTokenPath := appCfg.ServiceUrl + enum.TokenPath

		getTokenStatus, getTokenResponse := utils.HttpRawRequest[service.GetTokenResponse](context, getTokenPath, nil)

		if getTokenStatus != http.StatusOK {
			utils.LogRequestError(context, enum.GetTokenFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}

		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+getTokenResponse.Data.Token)
		userToken := RandomStringGenerator()
		if userToken == "" {
			utils.LogRequestError(context, enum.GenerateRandomStringFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}
		expirationTime := time.Now().Add(enum.UserTokenExpiredTime)
		name := profile[enum.OidcName].(string)
		username := profile[enum.OidcNickname].(string)

		var syncUserRequest = service.SyncUserRequest{
			Data: struct {
				AuthId     string `json:"authId"`
				Token      string `json:"token"`
				Username   string `json:"username"`
				Name       string `json:"name"`
				Role       int    `json:"role"`
				ExpiryDate string `json:"expiryDate"`
			}(struct {
				AuthId     string
				Token      string
				Username   string
				Name       string
				Role       int
				ExpiryDate string
			}{AuthId: userId,
				Token:      userToken,
				Username:   username,
				Name:       name,
				Role:       enum.Member,
				ExpiryDate: expirationTime.Format(time.RFC3339)}),
		}

		syncUserPath := appCfg.ServiceUrl + enum.SyncUserPath
		syncUserStatus, syncUserResponse := utils.HttpRawRequest[service.SyncUserResponse](context, syncUserPath, syncUserRequest)

		if syncUserStatus != http.StatusOK {
			utils.LogRequestError(context, enum.SyncUserFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}

		cache.Store.Set(userToken, userId, enum.UserTokenExpiredTime)
		cache.Store.Set(enum.ApiTokenKey+userToken, syncUserResponse.Data.Token, enum.ApiTokenExpiredTime)

		// Construct the redirect URL with the parameters
		redirectUrl := appCfg.DefaultRedirectUrl

		if currentUrl, ok := cache.Store.Get(enum.CurrentUrl); ok {
			redirectUrl = currentUrl
			cache.Store.Delete(enum.CurrentUrl)
		}

		role := enum.Member
		if StringInSlice(userId, moderatorIds) {
			role = enum.Moderator
		}

		constructedRedirectUrl := fmt.Sprintf("%s?userId=%s&role=%d&userToken=%s",
			redirectUrl, userId, role, userToken)

		// Redirect to the constructed URL
		context.Redirect(http.StatusTemporaryRedirect, constructedRedirectUrl)
	}
}

func RandomStringGenerator() string {
	randomBytes := make([]byte, 32)
	_, err := rand.Read(randomBytes)
	if err != nil {
		return ""
	}
	return base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(randomBytes)
}

func StringInSlice(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}
