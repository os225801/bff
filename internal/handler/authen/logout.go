package authen

import (
	"bff/config"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

func LogOutHandler(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {

		userToken := context.GetHeader(enum.HttpAuthorization)
		if userToken == "" {
			utils.ClientError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}
		cache.Store.Delete(userToken)

		context.Redirect(http.StatusTemporaryRedirect, appCfg.DefaultRedirectUrl)
	}
}
