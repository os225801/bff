package authen

import (
	"bff/config"
	"bff/internal/authenticator"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/utils"
	"crypto/rand"
	"encoding/base64"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"net/http"
	"time"
)

func LoginHandler(auth *authenticator.Authenticator, appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {

		currentURL := context.Query(enum.CurrentUrl)
		if currentURL != "" {
			cache.Store.Set(enum.CurrentUrl, currentURL, 5*time.Minute)
		}

		state, err := generateRandomState()
		if err != nil {
			utils.LogRequestError(context, enum.GenerateRandomStateFailed)
			utils.SystemError(context)
			context.Header(enum.HttpContentType, enum.HttpJson)
			return
		}

		context.Redirect(http.StatusTemporaryRedirect, auth.AuthCodeURL(state, oauth2.SetAuthURLParam(enum.OidcAuthUrlParamKey, appCfg.Auth0Audience)))
	}
}

func generateRandomState() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	state := base64.StdEncoding.EncodeToString(b)

	return state, nil
}
