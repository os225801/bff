package service

import (
	"bff/config"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/model/common"
	"bff/internal/model/service"
	"bff/internal/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateComment(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {

		err := utils.ValidateRequest[service.CreateCommentRequest](context)
		if err != nil {
			utils.ClientError(context)
			return
		}

		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.CreateCommentResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func DeleteComment(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.DeleteCommentResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetCommentByParentComment(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.GetCommentsByParentCommentResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetCommentById(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.GetCommentByIdResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetCommentByPost(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.GetCommentByPostResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func CountSubCommentsById(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.CountSubCommentsByIdResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}
