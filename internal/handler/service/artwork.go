package service

import (
	"bff/config"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/model/common"
	"bff/internal/model/service"
	"bff/internal/utils"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UploadArtwork(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UploadArtworkResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func UploadArtworkV2(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UploadArtworkResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func DownloadArtwork(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode, fileName := utils.HttpRequestDownload(context, appCfg.ServiceUrl)

		if statusCode == http.StatusInternalServerError {
			utils.SystemError(context)
			return
		}
		if len(response) == 0 {
			utils.ResourceError(context)
			return
		}

		context.Header(enum.HttpContentType, enum.HttpOctetStream)
		context.Header(enum.HttpContentDisposition, fmt.Sprintf(enum.AttachmentFormat, fileName))
		context.Data(statusCode, enum.HttpOctetStream, response)
	}
}

func DeleteArtwork(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.DeleteArtworkResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func HelloWorld() gin.HandlerFunc {
	return func(context *gin.Context) {
		var errorResponse = &common.Error{
			ResultCode:    "00",
			ResultMessage: "Hello World !",
		}
		context.JSON(200, errorResponse)
	}
}
