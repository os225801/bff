package service

import (
	"bff/config"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/model/common"
	"bff/internal/model/service"
	"bff/internal/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UpdateUserInformation(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UpdateUserInfoResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func UpdateUserImage(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UpdateUserImageResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetUserById(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if ok || apiToken != "" {
			context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		}
		response, statusCode := utils.HttpRequest[service.GetUserByIdResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetCurrentUser(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.GetCurrentUserResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetLoggedInUsers(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.GetLoggedInUsersResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

//func GetUsersPaginated() gin.HandlerFunc {
//	return func(context *gin.Context) {
//
//		var response service.GetUsersPaginatedResponse
//		httpStatus := utils.HttpRequest(context, nil, &response)
//
//		switch {
//		case httpStatus == http.StatusInternalServerError:
//			utils.SystemError(context)
//
//		case httpStatus != http.StatusOK:
//			var errorResponse = &common.Error{
//				ResultCode:    response.ResultCode,
//				ResultMessage: response.ResultMessage,
//			}
//			context.JSON(httpStatus, errorResponse)
//
//		default:
//			context.JSON(httpStatus, response)
//		}
//	}
//}

func CountAllUsers(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.CountAllUsersResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}
