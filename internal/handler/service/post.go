package service

import (
	"bff/config"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/model/common"
	"bff/internal/model/service"
	"bff/internal/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreatePost(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {

		err := utils.ValidateRequest[service.CreatePostRequest](context)
		if err != nil {
			utils.ClientError(context)
			return
		}

		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}

		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.CreatePostResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func SearchPost(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if ok || apiToken != "" {
			context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		}
		response, statusCode := utils.HttpRequest[service.SearchPostResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetPostsByCollection(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if ok || apiToken != "" {
			context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		}
		response, statusCode := utils.HttpRequest[service.GetPostsByCollectionResponse](context, appCfg.ServiceUrl)

		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetPostById(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if ok || apiToken != "" {
			context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		}
		response, statusCode := utils.HttpRequest[service.GetPostByIdResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetPostsByArtist(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if ok || apiToken != "" {
			context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		}
		response, statusCode := utils.HttpRequest[service.GetPostsByArtistResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func UpdatePost(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {

		err := utils.ValidateRequest[service.UpdatePostRequest](context)
		if err != nil {
			utils.ClientError(context)
			return
		}

		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UpdatePostResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func DeletePost(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.DeletePostResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func IncreasePostViews(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.IncreasePostViewsResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}
