package service

import (
	"bff/config"
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/model/common"
	"bff/internal/model/service"
	"bff/internal/utils"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateCollection(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		err := utils.ValidateRequest[service.CreateCollectionRequest](context)
		if err != nil {
			utils.ClientError(context)
			return
		}

		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.CreateCollectionResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func DeleteCollection(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.DeleteCollectionResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func UpdateCollectionInfo(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UpdateCollectionInfoResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func UpdateCollectionPost(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		err := utils.ValidateRequest[service.UpdateCollectionPostRequest](context)
		if err != nil {
			utils.ClientError(context)
			return
		}

		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.UpdateCollectionPostResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetSelfCollections(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		sessionToken := context.GetHeader(enum.HttpAuthorization)
		apiToken, ok := cache.Store.Get(enum.ApiTokenKey + sessionToken)
		if !ok {
			utils.SystemError(context)
			return
		}
		context.Request.Header.Set(enum.HttpAuthorization, enum.Bearer+apiToken)
		response, statusCode := utils.HttpRequest[service.GetSelfCollectionsResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetCollectionsPaginated(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.GetCollectionsPaginatedResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}

func GetCollectionDetails(appCfg *config.App) gin.HandlerFunc {
	return func(context *gin.Context) {
		response, statusCode := utils.HttpRequest[service.GetCollectionDetailsResponse](context, appCfg.ServiceUrl)
		switch statusCode {
		case http.StatusOK:
			context.JSON(statusCode, response)

		case http.StatusInternalServerError:
			utils.SystemError(context)

		default:
			var errorResponse = &common.Error{
				ResultCode:    response.ResultCode,
				ResultMessage: response.ResultMessage,
			}
			context.JSON(statusCode, errorResponse)
		}
	}
}
