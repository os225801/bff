package utils

import (
	"bff/internal/enum"
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/google/uuid"
	"io"
	"mime"
	"net/http"
)

var (
	client *http.Client
)

func init() {
	client = &http.Client{}
}

func HttpRequest[T any](context *gin.Context, serviceUrl string) (*T, int) {

	url := serviceUrl + context.Request.URL.String()

	req, err := http.NewRequest(context.Request.Method, url, context.Request.Body)
	if err != nil {
		LogRequestError(context, enum.CreateHttpRequestFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError
	}

	// Set Header
	req.Header.Set(enum.HttpContentType, context.Request.Header.Get(enum.HttpContentType))
	req.Header.Set(enum.RequestId, context.Request.Header.Get(enum.RequestId))
	req.Header.Set(enum.HttpAuthorization, context.Request.Header.Get(enum.HttpAuthorization))

	// Send Request
	response, err := client.Do(req)
	if err != nil {
		LogRequestError(context, enum.SendHttpRequestFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError
	}

	// Close response body
	var closeErr error
	defer func(Body io.ReadCloser) {
		closeErr = Body.Close()
		if closeErr != nil {
			LogRequestError(context, enum.CloseResponseBodyFailed)
		}
	}(response.Body)
	if closeErr != nil {
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError
	}
	var responseBody T
	// Decode the response body
	if err := json.NewDecoder(response.Body).Decode(&responseBody); err != nil {
		LogRequestError(context, enum.DecodeResponseBodyFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError
	}
	return &responseBody, response.StatusCode
}

func HttpRequestDownload(context *gin.Context, serviceUrl string) ([]byte, int, string) {

	url := serviceUrl + context.Request.URL.String()

	req, err := http.NewRequest(context.Request.Method, url, context.Request.Body)
	if err != nil {
		LogRequestError(context, enum.CreateHttpRequestFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError, ""
	}

	// Set Header
	req.Header.Set(enum.HttpContentType, context.Request.Header.Get(enum.HttpContentType))
	req.Header.Set(enum.RequestId, context.Request.Header.Get(enum.RequestId))
	req.Header.Set(enum.HttpAuthorization, context.Request.Header.Get(enum.HttpAuthorization))

	// Send Request
	response, err := client.Do(req)
	if err != nil {
		LogRequestError(context, enum.SendHttpRequestFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError, ""
	}

	// Close response body
	var closeErr error
	defer func(Body io.ReadCloser) {
		closeErr = Body.Close()
		if closeErr != nil {
			LogRequestError(context, enum.CloseResponseBodyFailed)
		}
	}(response.Body)
	if closeErr != nil {
		LogRequestError(context, enum.DecodeResponseBodyFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError, ""
	}

	_, params, err := mime.ParseMediaType(response.Header.Get(enum.HttpContentDisposition))
	if err != nil {
		LogRequestError(context, enum.ParseMediaTypeFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, response.StatusCode, ""
	}

	fileName := params[enum.Filename]
	if fileName == "" {
		LogRequestError(context, enum.ParseFileNameFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, response.StatusCode, ""
	}

	// Decode the response body
	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		LogRequestError(context, enum.DecodeResponseBodyFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return nil, http.StatusInternalServerError, ""
	}

	return bodyBytes, response.StatusCode, fileName
}

func HttpRawRequest[T any](context *gin.Context, path string, requestBody interface{}) (int, *T) {
	var responseBody T

	request := resty.New().
		SetJSONMarshaler(json.Marshal).
		SetJSONUnmarshaler(json.Unmarshal).
		NewRequest()

	response, err := request.
		SetHeader(enum.HttpContentType, enum.HttpJson).
		SetHeader(enum.HttpAuthorization, context.Request.Header.Get(enum.HttpAuthorization)).
		SetHeader(enum.RequestId, uuid.NewString()).
		SetBody(requestBody).
		Post(path)

	if err != nil {
		LogRequestError(context, enum.SendHttpRequestFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return http.StatusInternalServerError, nil
	}

	if err := json.NewDecoder(bytes.NewBuffer(response.Body())).Decode(&responseBody); err != nil {
		return http.StatusInternalServerError, nil
	}

	if err := response.RawResponse.Body.Close(); err != nil {
		LogRequestError(context, enum.CloseResponseBodyFailed)
		context.Header(enum.HttpContentType, enum.HttpJson)
		return http.StatusInternalServerError, nil
	}

	return response.StatusCode(), &responseBody
}
