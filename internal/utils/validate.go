package utils

import (
	"bff/internal/enum"
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"io"
)

var validate *validator.Validate

func init() {
	validate = validator.New(validator.WithRequiredStructEnabled())
}

func ValidateStruct(s interface{}) error {
	if err := validate.Struct(s); err != nil {
		return err
	}
	return nil
}

func BindAndValidateJSON(c *gin.Context, v interface{}) error {
	if err := c.BindJSON(v); err != nil {
		return err
	}
	if err := ValidateStruct(v); err != nil {
		return err
	}
	return nil
}

func ValidateRequest[T any](context *gin.Context) error {
	// Read the request body into a buffer
	bodyBytes, err := io.ReadAll(context.Request.Body)
	if err != nil {
		LogRequestError(context, enum.ReadRequestBodyFailed)
		return err
	}

	// Create a new io.ReadCloser from the buffer
	context.Request.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))

	// Check if the request is parsed correctly to the model
	var requestModel T
	if err := json.NewDecoder(bytes.NewBuffer(bodyBytes)).Decode(&requestModel); err != nil {
		LogRequestError(context, enum.ParseRequestFailed)
		return err
	}

	// Check required fields
	if err := validate.Struct(requestModel); err != nil {
		errMsg := fmt.Sprintf("%s (%v)", enum.ValidateRequiredFieldsFailed, err)
		LogRequestError(context, errMsg)
		return err
	}
	return nil
}
