package utils

import (
	"bff/internal/enum"
	"bff/internal/logger"
	"bff/internal/model/common"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
)

func StartUpError(l *zap.Logger, message string) {
	l.Fatal(message)
}

func SystemError(context *gin.Context) {
	var systemError = &common.Error{
		ResultCode:    enum.SystemErrorCode,
		ResultMessage: enum.SystemErrorMessage,
	}
	context.JSON(http.StatusInternalServerError, systemError)
}

func ClientError(context *gin.Context) {
	var clientError = &common.Error{
		ResultCode:    enum.ClientErrorCode,
		ResultMessage: enum.ClientErrorMessage,
	}
	context.JSON(http.StatusBadRequest, clientError)
}

func ResourceError(context *gin.Context) {
	var resourceError = &common.Error{
		ResultCode:    enum.ResourceErrorCode,
		ResultMessage: enum.ResourceErrorMessage,
	}
	context.JSON(http.StatusBadRequest, resourceError)
}

func UnauthorizedError(context *gin.Context) {
	var unAuthorizedError = &common.Error{
		ResultCode:    enum.UnAuthorizedErrorCode,
		ResultMessage: enum.UnAuthorizedErrorMessage,
	}
	context.JSON(http.StatusForbidden, unAuthorizedError)
}

func LogRequestError(context *gin.Context, errMsg string) {
	l := logger.FromCtx(context)
	l.Error(errMsg)
}
