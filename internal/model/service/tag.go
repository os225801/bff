package service

type GetTagByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id             string `json:"id"`
		TagName        string `json:"tagName"`
		TagDescription string `json:"tagDescription"`
	} `json:"data"`
}

type GetAllTagPaginatedResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id             string `json:"id"`
			TagName        string `json:"tagName"`
			TagDescription string `json:"tagDescription"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}
