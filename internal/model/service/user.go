package service

type UpdateUserInfoResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type UpdateUserImageResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type GetUserByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id              string `json:"id"`
		Username        string `json:"username"`
		Name            string `json:"name"`
		AvatarLink      string `json:"avatarLink"`
		BannerLink      string `json:"bannerLink"`
		TotalFollowers  int    `json:"totalFollowers"`
		TotalFollowings int    `json:"totalFollowings"`
		IsFollowed      bool   `json:"isFollowed"`
	} `json:"data"`
}

type GetCurrentUserResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id              string `json:"id"`
		Username        string `json:"username"`
		Name            string `json:"name"`
		AvatarLink      string `json:"avatarLink"`
		BannerLink      string `json:"bannerLink"`
		AccountNumber   string `json:"accountNumber"`
		BankName        string `json:"bankName"`
		AccountName     string `json:"accountName"`
		TotalFollowers  int    `json:"totalFollowers"`
		TotalFollowings int    `json:"totalFollowings"`
	} `json:"data"`
}

type CountAllUsersResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		TotalUsers int `json:"totalUsers"`
	} `json:"data"`
}

type GetLoggedInUsersResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id         string `json:"id"`
			Token      string `json:"token"`
			ExpiryDate string `json:"expiry_date"`
			CreatedAt  string `json:"created_at"`
			RoleId     int    `json:"role_id"`
			UserId     string `json:"user_id"`
			User       struct {
				UserId   string `json:"userId"`
				Username string `json:"username"`
				Name     string `json:"name"`
				Avatar   string `json:"avatar"`
			} `json:"user"`
			RowNum int `json:"row_num"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}
