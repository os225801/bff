package service

type GetWalletResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Balance int `json:"balance"`
	} `json:"data"`
}
