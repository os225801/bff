package service

import "time"

type CreateCommentRequest struct {
	Data struct {
		PostId          string `json:"postId" validate:"required"`
		ParentCommentId string `json:"parentCommentId"`
		Content         string `json:"content" validate:"required"`
	} `json:"data"`
}

type CreateCommentResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		CommentId string `json:"commentId"`
	} `json:"data"`
}

type DeleteCommentResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type GetCommentsByParentCommentResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id        string `json:"id"`
			Content   string `json:"content"`
			CreatedAt string `json:"createdAt"`
			PostId    string `json:"postId"`
			User      struct {
				UserId   string `json:"userId"`
				Username string `json:"username"`
				Name     string `json:"name"`
				Avatar   string `json:"avatar"`
			} `json:"user"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type GetCommentByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Content   string    `json:"content"`
		CreatedAt time.Time `json:"createdAt"`
		PostId    string    `json:"postId"`
		User      struct {
			UserId   string `json:"userId"`
			Username string `json:"username"`
			Name     string `json:"name"`
			Avatar   string `json:"avatar"`
		} `json:"user"`
	} `json:"data"`
}

type GetCommentByPostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id        string `json:"id"`
			Content   string `json:"content"`
			CreatedAt string `json:"createdAt"`
			PostId    string `json:"postId"`
			User      struct {
				UserId   string `json:"userId"`
				Username string `json:"username"`
				Name     string `json:"name"`
				Avatar   string `json:"avatar"`
			} `json:"user"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type CountSubCommentsByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Count int `json:"count"`
	} `json:"data"`
}
