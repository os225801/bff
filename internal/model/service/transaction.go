package service

import "time"

type WithdrawMoneyRequest struct {
	Data struct {
		Amount int `json:"amount" validate:"required"`
	} `json:"data"`
}

type WithdrawMoneyResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		TransactionId string `json:"transactionId"`
	} `json:"data"`
}

type UpdateWithdrawMoneyStatusRequest struct {
	Data struct {
		UserId string `json:"userId" validate:"required"`
		Status int    `json:"status" validate:"required"`
	} `json:"data"`
}

type UpdateWithdrawMoneyStatusResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type GetTransactionsHistoryResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id              string `json:"id"`
			Type            int    `json:"type"`
			Status          int    `json:"status"`
			Amount          int    `json:"amount"`
			UserId          string `json:"userId"`
			OrderId         string `json:"orderId"`
			TransactionDate string `json:"transactionDate"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type GetTransactionByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id              string    `json:"id"`
		Type            int       `json:"type"`
		Status          int       `json:"status"`
		TransactionDate time.Time `json:"transactionDate"`
		Amount          int       `json:"amount"`
		UserId          string    `json:"userId"`
	} `json:"data"`
}

type GetTransactionPaginatedResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id              string `json:"id"`
			Type            int    `json:"type"`
			Status          int    `json:"status"`
			Amount          int    `json:"amount"`
			UserId          string `json:"userId"`
			OrderId         string `json:"orderId"`
			TransactionDate string `json:"transactionDate"`
			ModeratorId     string `json:"moderator_id"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}
