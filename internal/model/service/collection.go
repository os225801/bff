package service

type GetCollectionDetailsResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id             string `json:"id"`
		CollectionName string `json:"collectionName"`
		Description    string `json:"description"`
		UserId         string `json:"userId"`
		Name           string `json:"name"`
		Username       string `json:"username"`
		AvatarLink     string `json:"avatarLink"`
	} `json:"data"`
}

type CreateCollectionRequest struct {
	Data struct {
		Name        string `json:"name" validate:"required"`
		Description string `json:"description"`
	} `json:"data"`
}

type CreateCollectionResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		CollectionId string `json:"collectionId"`
	} `json:"data"`
}

type UpdateCollectionPostRequest struct {
	Data struct {
		PostId string `json:"postId" validate:"required"`
	} `json:"data"`
}

type UpdateCollectionPostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		IsExisted bool `json:"isExisted"`
	} `json:"data"`
}

type DeleteCollectionResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type GetSelfCollectionsResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id          string  `json:"id"`
			Name        string  `json:"name"`
			Description string  `json:"description"`
			TotalPosts  int     `json:"totalPosts"`
			CoverImage  *string `json:"coverImage"`
			Status      bool    `json:"status"`
		} `json:"items"`
	} `json:"data"`
}

type UpdateCollectionInfoResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type GetCollectionsPaginatedResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id         string `json:"id"`
			Name       string `json:"name"`
			CoverImage []struct {
				ImageLink string `json:"imageLink"`
			} `json:"coverImage"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}
