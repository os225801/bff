package service

import "time"

type CreateOrderRequest struct {
	Data struct {
		ArtworkId string `json:"artworkId" validate:"required"`
	}
}

type CreateOrderResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		PaymentLink string `json:"paymentLink"`
	} `json:"data"`
}

type UpdateOrderStatusRequest struct {
	Data struct {
		UserID string `json:"userId" validate:"required"`
		Status int    `json:"status" validate:"required"`
	} `json:"data"`
}

type UpdateOrderStatusResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type GetOrderByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id          string      `json:"id"`
		OrderDate   time.Time   `json:"orderDate"`
		Amount      int         `json:"amount"`
		Status      int         `json:"status"`
		BuyerId     string      `json:"buyerId"`
		SellerId    string      `json:"sellerId"`
		ArtworkId   string      `json:"artworkId"`
		OrderCode   int         `json:"orderCode"`
		PaymentLink interface{} `json:"paymentLink"`
		Image       string      `json:"image"`
		OrderName   string      `json:"orderName"`
		PostId      string      `json:"postId"`
	} `json:"data"`
}

type GetOrdersHistoryResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id        string `json:"id"`
			Amount    int    `json:"amount"`
			OrderDate string `json:"orderDate"`
			Status    int    `json:"status"`
			BuyerId   string `json:"buyerId"`
			SellerId  string `json:"sellerId"`
			ArtworkId string `json:"artworkId"`
			Image     string `json:"image"`
			OrderName string `json:"orderName"`
			PostId    string `json:"postId"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type GetOrdersPaginatedResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id              string `json:"id"`
			Amount          int    `json:"amount"`
			OrderDate       string `json:"orderDate"`
			Status          int    `json:"status"`
			BuyerId         string `json:"buyerId"`
			SellerId        string `json:"sellerId"`
			TransferContent string `json:"transferContent"`
			ArtworkId       string `json:"artworkId"`
			OrderName       string `json:"orderName"`
			PostId          string `json:"postId"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}
