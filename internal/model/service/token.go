package service

type GetTokenResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Token string `json:"token"`
	} `json:"data"`
}

type SyncUserRequest struct {
	Data struct {
		AuthId     string `json:"authId"`
		Token      string `json:"token"`
		Username   string `json:"username"`
		Name       string `json:"name"`
		Role       int    `json:"role"`
		ExpiryDate string `json:"expiryDate"`
	} `json:"data"`
}

type SyncUserResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Token string `json:"token"`
	} `json:"data"`
}
