package service

import "time"

type CreatePostRequest struct {
	Data struct {
		Title       string   `json:"title" validate:"required"`
		Description string   `json:"description" validate:"required"`
		Price       int      `json:"price"`
		IsBuyable   bool     `json:"isBuyable"`
		ArtworkId   string   `json:"artworkId" validate:"required"`
		TagIds      []string `json:"tagIds" validate:"required"`
	} `json:"data"`
}

type CreatePostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		PostId string `json:"postId"`
	} `json:"data"`
}

type SearchPostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id          string `json:"id"`
			Title       string `json:"title"`
			Description string `json:"description"`
			CreatedAt   string `json:"createdAt"`
			Price       int    `json:"price"`
			Views       int    `json:"views"`
			TotalLikes  int    `json:"totalLikes"`
			User        struct {
				UserId   string `json:"userId"`
				Username string `json:"username"`
				Name     string `json:"name"`
				Avatar   string `json:"avatar"`
			} `json:"user"`
			Artwork struct {
				ArtworkId string `json:"artworkId"`
				Image     string `json:"image"`
				Type      int    `json:"type"`
				IsBuyable bool   `json:"isBuyable"`
			} `json:"artwork"`
			IsLiked bool `json:"isLiked"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type GetPostByIdResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Id          string    `json:"id"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		CreatedAt   time.Time `json:"createdAt"`
		Price       int       `json:"price"`
		Views       int       `json:"views"`
		User        struct {
			UserId   string `json:"userId"`
			Username string `json:"username"`
			Name     string `json:"name"`
			Avatar   string `json:"avatar"`
		} `json:"user"`
		Artwork struct {
			ArtworkId string `json:"artworkId"`
			Image     string `json:"image"`
			Type      int    `json:"type"`
			IsBuyable bool   `json:"isBuyable"`
		} `json:"artwork"`
		Tags []struct {
			TagId   string `json:"id"`
			TagName string `json:"tagName"`
		} `json:"tags"`
		TotalLikes   int  `json:"totalLikes"`
		IsLiked      bool `json:"isLiked"`
		IsDownloaded bool `json:"isDownloaded"`
	} `json:"data"`
}

type GetPostsByArtistResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id          string `json:"id"`
			Title       string `json:"title"`
			Description string `json:"description"`
			CreatedAt   string `json:"createdAt"`
			Price       int    `json:"price"`
			Views       int    `json:"views"`
			TotalLikes  int    `json:"totalLikes"`
			User        struct {
				UserId   string `json:"userId"`
				Username string `json:"username"`
				Name     string `json:"name"`
				Avatar   string `json:"avatar"`
			} `json:"user"`
			Artwork struct {
				ArtworkId string `json:"artworkId"`
				Image     string `json:"image"`
				Type      int    `json:"type"`
				IsBuyable bool   `json:"isBuyable"`
			} `json:"artwork"`
			IsLiked bool `json:"isLiked"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type UpdatePostRequest struct {
	Data struct {
		Title       string   `json:"title"`
		Description string   `json:"description"`
		Price       int      `json:"price"`
		IsBuyable   bool     `json:"isBuyable"`
		TagIds      []string `json:"tagIds" validate:"required"`
	} `json:"data"`
}

type UpdatePostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type DeletePostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}

type CountPostLikeResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		TotalLikes int `json:"totalLikes"`
	} `json:"data"`
}

type CountAllPostResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		TotalPost int `json:"totalPost"`
	} `json:"data"`
}

type GetPostsByCollectionResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
	Data          struct {
		Items []struct {
			Id          string `json:"id"`
			Title       string `json:"title"`
			Description string `json:"description"`
			CreatedAt   string `json:"createdAt"`
			Price       int    `json:"price"`
			Views       int    `json:"views"`
			TotalLikes  int    `json:"totalLikes"`
			User        struct {
				UserId   string `json:"userId"`
				Username string `json:"username"`
				Name     string `json:"name"`
				Avatar   string `json:"avatar"`
			} `json:"user"`
			Artwork struct {
				ArtworkId string `json:"artworkId"`
				Image     string `json:"image"`
				Type      int    `json:"type"`
				IsBuyable bool   `json:"isBuyable"`
			} `json:"artwork"`
			IsLiked bool `json:"isLiked"`
		} `json:"items"`
		TotalCount  int    `json:"totalCount"`
		HasNextPage bool   `json:"hasNextPage"`
		HasPrevPage bool   `json:"hasPrevPage"`
		TotalPages  int    `json:"totalPages"`
		PageNumber  int    `json:"pageNumber"`
		PageSize    int    `json:"pageSize"`
		SortBy      string `json:"sortBy"`
		SortOrder   string `json:"sortOrder"`
	} `json:"data"`
}

type IncreasePostViewsResponse struct {
	ResultCode    string `json:"resultCode"`
	ResultMessage string `json:"resultMessage"`
}
