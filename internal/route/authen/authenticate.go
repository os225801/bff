package authen

import (
	"bff/config"
	"bff/internal/authenticator"
	"bff/internal/handler/authen"
	"github.com/gin-gonic/gin"
)

func Authenticate(auth *authenticator.Authenticator, router *gin.Engine, appCfg *config.App) {
	router.GET("/login", authen.LoginHandler(auth, appCfg))
	router.GET("/callback", authen.CallBackHandler(auth, appCfg))
	router.GET("/logout", authen.LogOutHandler(appCfg))
}
