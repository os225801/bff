package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const (
	likePath = "/likes"
)

func LikeRoutes(router *gin.Engine, appCfg *config.App) {
	router.POST(postIdPath+likePath, middleware.IsAuthenticated, service.HandleLikePost(appCfg))
}
