package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const userPath = "/users"
const userIdPath = userPath + "/:userId"

func UserRoutes(router *gin.Engine, appCfg *config.App) {
	router.PUT(userPath+"/info", middleware.IsAuthenticated, service.UpdateUserInformation(appCfg))
	router.PUT(userPath+"/image", middleware.IsAuthenticated, service.UpdateUserImage(appCfg))
	//router.GET(userPath+"/moderator", middleware.IsAuthenticated, service.GetUsersPaginated())
	//router.GET(userPath, middleware.IsAuthenticated, service.GetCurrentUserInformation())
	router.GET(userPath+"/count", middleware.IsAuthenticated, service.CountAllUsers(appCfg))
	router.GET(userPath+"/current-user", middleware.IsAuthenticated, service.GetCurrentUser(appCfg))
	router.GET(userPath, middleware.IsAuthenticated, service.GetLoggedInUsers(appCfg))
	router.GET(userIdPath, service.GetUserById(appCfg))
}
