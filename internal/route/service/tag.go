package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"github.com/gin-gonic/gin"
)

func TagRoutes(router *gin.Engine, appCfg *config.App) {
	router.GET("/tags", service.GetAllTagPaginated(appCfg))
	router.GET("/tags/:tagId", service.GetTagById(appCfg))
}
