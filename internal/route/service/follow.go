package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const followPath = "/follows"

func FollowRoutes(router *gin.Engine, appCfg *config.App) {
	router.POST(userIdPath+followPath, middleware.IsAuthenticated, service.HandleFollowUser(appCfg))
}
