package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const transactionPath = "/transactions"
const transactionIDPath = transactionPath + "/:transactionId"

func TransactionRoutes(router *gin.Engine, appCfg *config.App) {
	router.GET(transactionIDPath, middleware.IsAuthenticated, service.GetTransactionById(appCfg))
	router.GET("/users/transactions", middleware.IsAuthenticated, service.GetTransactionsHistory(appCfg))
	router.POST(transactionPath, middleware.IsAuthenticated, service.WithdrawMoney(appCfg))
	router.PUT(transactionIDPath, middleware.IsAuthenticated, service.UpdateWithdrawMoneyStatus(appCfg))
	router.GET(transactionPath, middleware.IsAuthenticated, service.GetTransactionPaginated(appCfg))
}
