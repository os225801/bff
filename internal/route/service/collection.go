package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const collectionPath = "/collections"
const collectionIdPath = collectionPath + "/:collectionId"

func CollectionRoutes(router *gin.Engine, appCfg *config.App) {

	router.POST(collectionPath, middleware.IsAuthenticated, service.CreateCollection(appCfg))
	router.PUT(collectionIdPath, middleware.IsAuthenticated, service.UpdateCollectionInfo(appCfg))
	router.DELETE(collectionIdPath, middleware.IsAuthenticated, service.DeleteCollection(appCfg))
	router.PUT(collectionIdPath+postPath, middleware.IsAuthenticated, service.UpdateCollectionPost(appCfg))

	router.GET(collectionPath+postIdPath, middleware.IsAuthenticated, service.GetSelfCollections(appCfg))
	router.GET(collectionPath+userIdPath, service.GetCollectionsPaginated(appCfg))
	router.GET(collectionIdPath, service.GetCollectionDetails(appCfg))
}
