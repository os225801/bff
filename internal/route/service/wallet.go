package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

func WalletRoutes(router *gin.Engine, appCfg *config.App) {
	router.GET("/wallets", middleware.IsAuthenticated, service.GetWallet(appCfg))
}
