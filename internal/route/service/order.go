package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const orderPath = "/orders"
const orderIdPath = orderPath + "/:orderId"

func OrderRoutes(router *gin.Engine, appCfg *config.App) {
	router.POST(orderPath, middleware.IsAuthenticated, service.CreateOrder(appCfg))
	router.GET(orderIdPath, middleware.IsAuthenticated, service.GetOrderById(appCfg))
	router.PUT(orderIdPath, middleware.IsAuthenticated, service.UpdateOrderStatusResponse(appCfg))
	router.GET(orderPath, middleware.IsAuthenticated, service.GetOrdersPaginated(appCfg))
	router.GET(userPath+orderPath, middleware.IsAuthenticated, service.GetOrdersHistory(appCfg))
}
