package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const postPath = "/posts"
const postIdPath = postPath + "/:postId"

func PostRoutes(router *gin.Engine, appCfg *config.App) {

	router.POST(postPath, middleware.IsAuthenticated, service.CreatePost(appCfg))
	router.POST(postIdPath+"/views", service.IncreasePostViews(appCfg))
	router.PUT(postIdPath, middleware.IsAuthenticated, service.UpdatePost(appCfg))
	router.DELETE(postIdPath, middleware.IsAuthenticated, service.DeletePost(appCfg))

	router.GET(postPath, service.SearchPost(appCfg))
	router.GET(postIdPath, service.GetPostById(appCfg))
	router.GET(postPath+userIdPath, service.GetPostsByArtist(appCfg))
	router.GET(postPath+collectionIdPath, service.GetPostsByCollection(appCfg))
}
