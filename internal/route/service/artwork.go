package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const artworkPath = "/artworks"
const artworkIdPath = artworkPath + "/:artworkId"

func ArtworkRoutes(router *gin.Engine, appCfg *config.App) {
	router.POST(artworkPath, middleware.IsAuthenticated, service.UploadArtwork(appCfg))
	router.POST(artworkPath+"/v2", service.UploadArtworkV2(appCfg))

	router.DELETE(artworkIdPath, middleware.IsAuthenticated, service.DeleteArtwork(appCfg))
	router.GET(artworkIdPath+"/download", service.DownloadArtwork(appCfg))

	router.GET("hello", service.HelloWorld())
}
