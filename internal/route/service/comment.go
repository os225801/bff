package service

import (
	"bff/config"
	"bff/internal/handler/service"
	"bff/internal/middleware"
	"github.com/gin-gonic/gin"
)

const (
	commentPath   = "/comments"
	commentIdPath = "/comments/:commentId"
)

func CommentRoutes(router *gin.Engine, appCfg *config.App) {

	router.POST(commentPath, middleware.IsAuthenticated, service.CreateComment(appCfg))
	router.DELETE(commentIdPath, middleware.IsAuthenticated, service.DeleteComment(appCfg))

	router.GET(commentPath+"/parent-comments/:parentCommentId", service.GetCommentByParentComment(appCfg))
	router.GET(commentIdPath, service.GetCommentById(appCfg))
	router.GET(commentPath+"/posts/:postId", service.GetCommentByPost(appCfg))
	router.GET(commentIdPath+"/sub-comments/count", service.CountSubCommentsById(appCfg))

}
