package middleware

import (
	"bff/internal/cache"
	"bff/internal/enum"
	"bff/internal/utils"
	"github.com/gin-gonic/gin"
)

func IsAuthenticated(context *gin.Context) {

	userToken := context.GetHeader(enum.HttpAuthorization)

	if userToken == "" {
		utils.ClientError(context)
		context.Abort()
		context.Header(enum.HttpContentType, enum.HttpJson)
		return
	}

	if _, ok := cache.Store.Get(userToken); !ok {
		utils.UnauthorizedError(context)
		context.Abort()
		context.Header(enum.HttpContentType, enum.HttpJson)
		return
	}

	context.Next()
}
