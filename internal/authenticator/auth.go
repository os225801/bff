package authenticator

import (
	"bff/config"
	"context"
	"errors"
	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"
)

type Authenticator struct {
	*oidc.Provider
	oauth2.Config
}

func New() (*Authenticator, error) {
	auth0Cfg := config.Auth0Cfg()

	provider, err := oidc.NewProvider(
		context.Background(),
		"https://"+auth0Cfg.Domain+"/",
	)

	if err != nil {
		return nil, err
	}

	oauth2Cfg := oauth2.Config{
		ClientID:     auth0Cfg.ClientId,
		ClientSecret: auth0Cfg.ClientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  auth0Cfg.CallBackUrl,
		Scopes:       []string{oidc.ScopeOpenID, "profile", "offline_access"},
	}

	return &Authenticator{
		Provider: provider,
		Config:   oauth2Cfg,
	}, nil
}

func (a *Authenticator) VerifyIdToken(ctx context.Context, token *oauth2.Token) (*oidc.IDToken, error) {
	rawIdToken, ok := token.Extra("id_token").(string)

	if !ok {
		return nil, errors.New("no id_token field in oauth2 token")
	}

	oidcCfg := &oidc.Config{
		ClientID: a.ClientID,
	}

	return a.Verifier(oidcCfg).Verify(ctx, rawIdToken)
}
