package enum

const (
	HttpOctetStream        = "application/octet-stream"
	HttpJson               = "application/json"
	HttpContentType        = "Content-Type"
	HttpAuthorization      = "Authorization"
	HttpContentDisposition = "Content-Disposition"
)
