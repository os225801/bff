package enum

import "time"

var (
	CorsAllowMethods = []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}
	CorsAllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization", "X-CSRF-Token", "X-Request-ID",
		"Access-Control-Allow-Origin"}
	CorsMaxAge = 12 * time.Hour
)

const (
	EnvironmentFile  = ".env"
	ServiceUrl       = "SERVICE_URL"
	RedirectLocation = "REDIRECT_LOCATION"
	RequestId        = "X-Request-ID"
	CurrentUrl       = "current-url"
	Filename         = "filename"
	AttachmentFormat = "attachment; filename=%s"
)
