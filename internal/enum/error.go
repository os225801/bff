package enum

//goland:noinspection GoCommentStart
const (
	// common error codes
	SystemErrorCode       = "99"
	ClientErrorCode       = "01"
	ResourceErrorCode     = "02"
	UnAuthorizedErrorCode = "03"

	// common error messages
	SystemErrorMessage       = "System Error"
	ClientErrorMessage       = "Invalid Request"
	ResourceErrorMessage     = "Could not get resource !"
	UnAuthorizedErrorMessage = "User is not authorized !"

	// errors' log messages
	SetupOidcFailed              = "Setup OIDC failed !"
	ApplicationStartFailed       = "Application Start Failed !"
	CacheStoreStartFailed        = "Cache Store Start Failed !"
	SendHttpRequestFailed        = "Send request failed !"
	CloseResponseBodyFailed      = "Close response body failed !"
	ReadRequestBodyFailed        = "Read request body failed !"
	ParseRequestFailed           = "Parse request failed !"
	ValidateRequiredFieldsFailed = "Validate required fields failed !"
	GenerateRandomStateFailed    = "Generate random state failed !"
	ExchangeOidcTokenFailed      = "Exchange OIDC token failed !"
	VerifyIdTokenFailed          = "Verify ID token failed !"
	GetUserProfileFailed         = "Get user profile failed !"
	GetUserIdFailed              = "Get user id failed !"
	GenerateRandomStringFailed   = "Generate random string failed !"
	GetTokenFailed               = "Get token failed !"
	SyncUserFailed               = "Sync user failed !"
	DecodeResponseBodyFailed     = "Decode response body failed !"
	CreateHttpRequestFailed      = "Create http request failed !"
	ParseMediaTypeFailed         = "Parse media type failed !"
	ParseFileNameFailed          = "Parse file name failed !"
)
