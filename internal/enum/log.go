package enum

const (
	LogFolder       = "logs"
	LogFileName     = "app.log"
	LogTimeKey      = "timestamp"
	LogStartMessage = "Request started"
	LogEndMessage   = "Request completed"
	LogRequestId    = "requestId"
	LogMethod       = "method"
	LogUrl          = "url"
	LogStatusCode   = "statusCode"
	LogElapsedTime  = "elapsedTime"
	LogRequestBody  = "requestBody"
	LogResponseBody = "responseBody"
)
