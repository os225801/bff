package enum

import (
	"time"
)

const (
	OidcAuthUrlParamKey  = "audience"
	OidcExchangeCode     = "code"
	OidcUserId           = "sub"
	OidcName             = "name"
	OidcNickname         = "nickname"
	UserTokenExpiredTime = 7 * 24 * time.Hour
	ApiTokenExpiredTime  = 24 * time.Hour
	ApiTokenKey          = "api-token"
	Bearer               = "Bearer "
	SyncUserPath         = "/user/sync"
	TokenPath            = "/token"
)
