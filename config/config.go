package config

import (
	"bff/config/enum"
	"bff/internal/logger"
	"bff/internal/utils"
	"github.com/joho/godotenv"
)

func LoadAllConfig(envFile string) {
	err := godotenv.Load(envFile)

	if err != nil {
		l := logger.Get()
		utils.StartUpError(l, enum.LoadEnvFileFailed)
	}

	LoadAuth0()
	LoadApp()
}
