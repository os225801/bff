package config

import (
	"bff/config/enum"
	"os"
	"strconv"
)

type App struct {
	Host                string
	Port                int
	Auth0Audience       string
	CorsOriginDev       string
	CorsOriginProd      string
	CorsOriginModerator string
	SessionKey          string
	DefaultRedirectUrl  string
	ServiceUrl          string
}

var app = &App{}

func AppCfg() *App {
	return app
}

func LoadApp() {
	app.Host = os.Getenv(enum.AppHost)
	app.Port, _ = strconv.Atoi(os.Getenv(enum.AppPort))
	app.Auth0Audience = os.Getenv(enum.Auth0Audience)
	app.CorsOriginDev = os.Getenv(enum.CorsOriginDev)
	app.CorsOriginProd = os.Getenv(enum.CorsOriginProd)
	app.CorsOriginModerator = os.Getenv(enum.CorsOriginModerator)
	app.SessionKey = os.Getenv(enum.SessionKey)
	app.DefaultRedirectUrl = os.Getenv(enum.DefaultRedirectUrl)
	app.ServiceUrl = os.Getenv(enum.ServiceUrl)
}
