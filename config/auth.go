package config

import (
	"bff/config/enum"
	"os"
)

type Auth0 struct {
	Domain       string
	ClientId     string
	ClientSecret string
	CallBackUrl  string
}

var auth0 = &Auth0{}

func Auth0Cfg() *Auth0 {
	return auth0
}

func LoadAuth0() {
	auth0.Domain = os.Getenv(enum.Auth0Domain)
	auth0.ClientId = os.Getenv(enum.Auth0ClientId)
	auth0.ClientSecret = os.Getenv(enum.Auth0ClientSecret)
	auth0.CallBackUrl = os.Getenv(enum.Auth0CallBackUrl)
}
