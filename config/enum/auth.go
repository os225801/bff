package enum

const (
	Auth0Domain       = "AUTH0_DOMAIN"
	Auth0ClientId     = "AUTH0_CLIENT_ID"
	Auth0ClientSecret = "AUTH0_CLIENT_SECRET"
	Auth0CallBackUrl  = "AUTH0_CALLBACK_URL"
)
