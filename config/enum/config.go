package enum

const (
	AppHost             = "APP_HOST"
	AppPort             = "APP_PORT"
	Auth0Audience       = "AUTH0_AUDIENCE"
	CorsOriginDev       = "CORS_ORIGIN_DEVELOPMENT"
	CorsOriginProd      = "CORS_ORIGIN_PRODUCTION"
	CorsOriginModerator = "CORS_ORIGIN_MODERATOR"
	SessionKey          = "SESSION_KEY"
	CsrfKey             = "CSRF_KEY"
	DefaultRedirectUrl  = "DEFAULT_REDIRECT_URL"
	ServiceUrl          = "SERVICE_URL"
)
