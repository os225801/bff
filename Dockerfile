# Stage 1: Build stage
FROM golang:1.22.2-alpine AS builder

# Set the working directory
WORKDIR /build

# Copy and download dependencies
COPY go.mod go.sum ./
RUN go mod download && go mod verify

# Copy the source code and set environments
COPY ./config ./config
COPY ./internal ./internal
COPY ./main.go ./main.go
COPY ./.env ./.env
ENV GO111MODULE=on
ENV GOCACHE=/root/.cache/go-build

# Builds the application as a staticly linked one, to allow it to run on alpine.
RUN --mount=type=cache,target="/root/.cache/go-build" CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bff .

# Stage 2: Final stage
FROM alpine:edge

# Copy the binary from the build stage
COPY --from=builder ["/build/bff", "/build/.env" ,"/"]

# Set the timezone and install CA certificates
RUN apk --no-cache add ca-certificates tzdata

# Use nonroot user
RUN addgroup -S nonroot \
    && adduser -S nonroot -G nonroot
USER nonroot

# Set the entrypoint command
ENTRYPOINT ["/bff"]
EXPOSE 3001